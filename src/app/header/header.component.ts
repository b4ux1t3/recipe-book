import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { User } from '../auth/user.model';
import { DataStorageService } from '../shared/data-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  collapsed: boolean;
  loggedIn: boolean;
  user: User;
  private loggedInSub: Subscription;
  private userSub: Subscription;

  constructor(private datastorage: DataStorageService, private auth: AuthService) {
    this.collapsed = true;
  }
  ngOnInit(): void {
    this.loggedIn = false;
    this.loggedInSub = this.auth.loggedInSubject.subscribe((loggedInState) => this.loggedIn = loggedInState);
    this.userSub = this.auth.userSubject.subscribe((user: User) => this.user = user);
  }

  ngOnDestroy(): void {
    this.loggedInSub.unsubscribe();
  }

  saveData(): void {
    this.datastorage.saveRecipes();
  }

  fetchData(): void {
    this.datastorage.fetchRecipes().subscribe();
  }

  onLogout(): void {
    this.auth.logout();
  }
}
