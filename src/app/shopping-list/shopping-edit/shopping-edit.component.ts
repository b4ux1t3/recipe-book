import { Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListComponent } from '../shopping-list.component';
import { ShoppingListService } from '../shopping-list.service';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
    @ViewChild("unitSelect", {static: false}) unitSelect: ElementRef;
    @ViewChild("ourForm", {static: false}) ourForm: NgForm;


    @Output() formReset: EventEmitter<null>;

    editMode = false;
    edittedItem: Ingredient;

    ShoppingListEditSub: Subscription;

    currentId: number;

  constructor(private shoppingListService: ShoppingListService) {
    this.formReset = new EventEmitter<null>();

  }
  ngOnDestroy(): void {
    this.ShoppingListEditSub.unsubscribe();
  }

  ngOnInit(): void {
      this.ShoppingListEditSub = this.shoppingListService.startedEditing.subscribe((id) => {
        this.currentId = id;
        this.editMode = true;
        this.edittedItem = this.shoppingListService.getIngredient(id);
        this.ourForm.setValue({
          name: this.edittedItem.name,
          amount: this.edittedItem.measurement,
          unit: this.edittedItem.unit,
        });
      });

  }


  addClicked(form: NgForm): void{
    const value = form.value;

    const newIngredient = new Ingredient(value.name, value.amount, value.unit);
    if (this.editMode) this.shoppingListService.updateIngredient(this.currentId, newIngredient);
    else this.shoppingListService.addIngredient(newIngredient);

    this.resetForm();
  }

  onDelete(): void {
    this.shoppingListService.delete(this.currentId);
    this.resetForm();
  }

  resetForm(): void {
    this.ourForm.reset();
    this.editMode = false;
    this.currentId = null;
    this.formReset.emit();
  }
}

