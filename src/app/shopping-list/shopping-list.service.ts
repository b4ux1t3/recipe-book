import { Injectable, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {
  private ingredients: Ingredient[] = [];
  @Output() ingredientChanged: Subject<Ingredient[]> = new Subject<Ingredient[]>();

  startedEditing = new Subject<number>();
  constructor() { }
  getIngredient(id: number): Ingredient {
    return this.ingredients[id];
  }

  getIngredients() : Ingredient[]{
      return this.ingredients.slice();
  }

  addIngredient(newIngredient: Ingredient): void{
      this.ingredients.push(newIngredient);
      this.pushUpdatedList();
  }

  addIngredients(newIngredients: Ingredient[]) : void {
    newIngredients.forEach((ingredient) => this.ingredients.push(new Ingredient(ingredient.name, ingredient.measurement, ingredient.unit)));
    this.pushUpdatedList();
  }

  private pushUpdatedList() {
    this.ingredientChanged.next(this.ingredients.slice());
  }

  updateIngredient(id: number, ingredient: Ingredient): void {
    this.ingredients[id] = ingredient;
    this.pushUpdatedList();
  }

  delete(id: number)
  {
    this.ingredients.splice(id, 1);
    this.pushUpdatedList();
  }
}
