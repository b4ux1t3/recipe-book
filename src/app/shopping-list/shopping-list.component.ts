import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from './shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
  animations: [
    trigger('listGroup', [
      state('normal', style({
        transform: 'translateX(0)',
      })),
      state('hovered', style({
        transform: 'translateX(30px)',
      })),
      transition('normal <=> hovered', animate(300)),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }),
        animate(300)
      ]),
      transition('* => void', [
        animate(300, style({
          opacity: 0,
          transform: 'translateX(100px)'
        }))
      ])
    ])
  ]
})
export class ShoppingListComponent implements OnInit, OnDestroy {

    ingredients: Ingredient[] = [];
    private ingredientsSub: Subscription;
    public hoverStates: string[];

    public selectedId;

    constructor(private shoppingListService: ShoppingListService) { }
    ngOnDestroy(): void {
        this.ingredientsSub.unsubscribe();
    }


    ngOnInit(): void {
        this.ingredients = this.shoppingListService.getIngredients();
        this.ingredientsSub = this.shoppingListService.ingredientChanged.subscribe(
          (ingredients: Ingredient[]) => {
              this.ingredients = ingredients;
          });
      this.hoverStates = [];
      this.ingredients.forEach(element => this.hoverStates.push('normal'));
    }

    onEditItem(id: number): void {
      this.selectedId = id;
      this.shoppingListService.startedEditing.next(id);
    }

    clearSelect(): void{
      this.selectedId = null;
    }

    onHover(index: number): void {
      this.hoverStates[index] = 'hovered';
    }

    onLeave(index: number): void {
      this.hoverStates[index] = 'normal';
    }

}
