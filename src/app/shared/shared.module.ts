import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertComponent } from "./alert/alert.component";
import { DropdownDirective } from "./directives/dropdown.directive";
import { GreaterThanZeroDirective } from "./directives/greater-than-zero.directive";

@NgModule({
  declarations: [
    DropdownDirective,
    GreaterThanZeroDirective,
    AlertComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [
    DropdownDirective,
    GreaterThanZeroDirective,
    AlertComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

  ]
})
export class SharedModule {

}
