import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[appGreaterThanZero]',
  providers: [{provide: NG_VALIDATORS, useExisting: GreaterThanZeroDirective, multi: true}]
})
export class GreaterThanZeroDirective implements Validator{

  constructor() { }
  validate(control: AbstractControl): ValidationErrors | null{
    if (control.value < 1) return {'lessThanZero': false};
    return null
  }
  // registerOnValidatorChange?(fn: () => void): void {
  //   throw new Error('Method not implemented.');
  // }
}
