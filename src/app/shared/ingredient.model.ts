export class Ingredient {
    public name: string;
    public measurement: number;
    public unit: string;

    constructor(name: string, measurement: number, unit: string ){
        this.name = name;
        this.measurement = measurement;
        this.unit = unit;
    }

    public GetString(): string {
      return `${this.name} (${this.measurement}${this.unit})`;
    }
}

