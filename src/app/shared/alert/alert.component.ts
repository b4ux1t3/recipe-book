import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { authMessage } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent {
  @Input() message: authMessage;
  @Output() closeModal: EventEmitter<null> = new EventEmitter<null>();
  constructor() { }

  onClose(): void {
    this.closeModal.next();
  }
}
