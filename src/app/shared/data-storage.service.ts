import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Recipe } from '../recipe-book/recipe.model';
import { FirebaseRecord } from './firebase-record.model';
import { map, tap } from 'rxjs/operators';
import { RecipeService } from '../recipe-book/recipe.service';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { FirebaseCredentials } from '../auth/firebase-credentials.model';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  private readonly recipesUrl = 'https://udemy-angular-http-54889-default-rtdb.firebaseio.com/recipes.json';
  private readonly usersUrl = 'https://udemy-angular-http-54889-default-rtdb.firebaseio.com/users.json';

  constructor(private http: HttpClient, private recipeService: RecipeService) {}

  fetchRecipes(): Observable<Recipe[]> {
    return this.getRecipes();
  }

  saveRecipe(recipe: Recipe): boolean {
    let result: boolean;
    this.http.post<FirebaseRecord>(this.recipesUrl, recipe)
      .subscribe({
        next: (returnedData: FirebaseRecord) => {
        console.log(`Successfully uploaded new recipe to database: ${returnedData.name}`);
        result = true;
        },
        error: (error: HttpErrorResponse) => {
          console.log(`Error uploading recipe: ${recipe.name}\n${error.message}`);
          result = false;
        }
    });

    return result;
  }

  saveRecipes(): boolean {
    let result: boolean;
    this.http.put(this.recipesUrl, this.recipeService.getRecipes()).subscribe({
      next: (response) => {
        result = true;
      },
      error: (error) => {
        console.log(error);
        result = false;
      },
    });

    return result;
  }

  saveUser(creds: FirebaseCredentials, displayName: string): boolean {
    let result: boolean;
    this.http.post<FirebaseRecord>(this.usersUrl, {
      id: creds.localId,
      displayName: displayName,
    }).subscribe({
      next: (response) => {
        result = true;
      },
      error: () => {
        result = false;
      },
    });

    return result;
  }

  getUserDisplayName(id: string): Observable<string> {
    return this.http.get<any>(this.usersUrl).pipe(map((response) => {
      const keys = Object.keys(response);
      for (const key of keys) {
        if (response[key].id === id) return response[key].displayName;
      }
      return null;
    }));
  }

  private getRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.recipesUrl)
    .pipe(
      map((recipes: Recipe[]) => {
        return recipes.map((recipe) => {
          return ({
            ...recipe,
            ingredients: recipe.ingredients? recipe.ingredients : [],
          } as Recipe)
        });
      }),
      tap((recipes) => this.recipeService.replaceAllRecipes(recipes)),
    );
  }
}
