import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { environment } from "src/environments/environment";

const appRoutes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full'},
  {
    path: 'recipes',
    loadChildren: () => import('./recipe-book/recipe-book.module')
      .then(m => m.RecipeBookModule)
    },
  {
    path: 'list',
    loadChildren: () => import('./shopping-list/shopping-list.module')
      .then(m => m.ShoppingListModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module')
      .then(m => m.AuthModule)
  },

]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, {
          useHash: environment.hash,
          preloadingStrategy: PreloadAllModules,
        }),
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {


}
