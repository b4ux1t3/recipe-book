import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "../auth/auth.guard";
import { RecipeBookComponent } from "./recipe-book.component";
import { RecipeDetailsComponent } from "./recipe-details/recipe-details.component";
import { RecipeEditComponent } from "./recipe-details/recipe-edit/recipe-edit.component";
import { RecipeResolverService } from "./recipe-resolver.service";
import { RecipeGuard } from "./recipe.guard";

const recipeBookRoutes: Routes = [
  {
    path: '',
    component: RecipeBookComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'new', component: RecipeEditComponent },
      { path: ':id', component: RecipeDetailsComponent, canActivate: [RecipeGuard], resolve: [RecipeResolverService] },
      { path: ':id/edit', component: RecipeEditComponent, canActivate: [RecipeGuard], resolve: [RecipeResolverService] },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(recipeBookRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class RecipeBookRoutingModule {

}
