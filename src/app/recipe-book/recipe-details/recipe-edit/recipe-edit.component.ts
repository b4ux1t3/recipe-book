import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { Recipe } from '../../recipe.model';
import { RecipeService } from '../../recipe.service';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: number;
  editMode = false;

  recipeForm: FormGroup;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.initForm();
      }
    );
  }

  private initForm(): void {
    let recipeName = '';
    let imageURL = '';
    let description = '';

    const recipeIngredients = new FormArray([]);

    if (this.editMode) {
      const recipe = this.recipeService.getRecipe(this.id);
      recipeName = recipe.name;
      imageURL = recipe.imagePath;
      description = recipe.description;

      if (recipe.ingredients.length > 0) {
        recipe.ingredients.forEach((ingredient: Ingredient) => {
          recipeIngredients.push(new FormGroup({
            'ingredientName': new FormControl(ingredient.name, Validators.required),
            'amount': new FormControl(ingredient.measurement, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
            'unit':new FormControl(ingredient.unit, Validators.required)
          }));
        });
      }
    }

    this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, Validators.required),
      'image-url':  new FormControl(imageURL, Validators.required),
      'description': new FormControl(description, Validators.required),
      'ingredients': recipeIngredients
    });
  }

  get controls() { // a getter!
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }

  addIngredient(): void {
    (<FormArray> this.recipeForm.get('ingredients')).push(new FormGroup({
      'ingredientName': new FormControl(null, Validators.required),
      'amount': new FormControl(0, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
      'unit': new FormControl(null, Validators.required),
    }));
  }

  onSave(): void {
    const ingredients: Ingredient[] = [];
    const ingredientForms = this.recipeForm.get('ingredients') as FormArray;

    for (let i = 0; i < ingredientForms.length; i++) {
      const name = ingredientForms.controls[i].get('ingredientName').value;
      const amount = ingredientForms.controls[i].get('amount').value;
      const unit = ingredientForms.controls[i].get('unit').value;
      ingredients.push(new Ingredient(name, amount, unit));
    }

    const id = this.editMode ? this.id : this.recipeService.getCurrentId();
    const currentRecipe = new Recipe(this.recipeForm.get('name').value, this.recipeForm.get('description').value, this.recipeForm.get('image-url').value, ingredients, id);

    console.log(currentRecipe);
    if (this.editMode){
      this.recipeService.updateRecipe(this.id, currentRecipe);
    } else {
      this.recipeService.addRecipe(currentRecipe);
    }
    if (this.editMode) this.router.navigate([this.id], {relativeTo:this.route.parent});
    else this.router.navigate([currentRecipe.id], {relativeTo:this.route.parent})
    this.recipeForm.reset();
    this.initForm();
  }

  onCancel(): void {
    this.router.navigate(['..'], {relativeTo:this.route});
  }

  onDeleteIngredient(index: number): void {
    (<FormArray> this.recipeForm.get('ingredients')).removeAt(index);
  }
}
