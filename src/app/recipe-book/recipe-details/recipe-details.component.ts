import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from 'src/app/shopping-list/shopping-list.service';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  recipe: Recipe;
  ingredients: Ingredient[];
  constructor(private shoppingListService: ShoppingListService, private recipeService: RecipeService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.recipe = this.recipeService.getRecipe(+this.route.snapshot.params['id']);
    // if (this.recipe === undefined) {
    //   this.router.navigate(['/recipes']);
    //   return;
    // }
    this.ingredients = [];
    this.recipe.ingredients.forEach((ingredient) => this.ingredients.push(new Ingredient(ingredient.name, ingredient.measurement, ingredient.unit)));
    this.route.params.subscribe(
      (params: Params) => {
        this.recipe = this.recipeService.getRecipe(+params['id']);
        this.ingredients = [];
        this.recipe.ingredients.forEach((ingredient) => this.ingredients.push(new Ingredient(ingredient.name, ingredient.measurement, ingredient.unit)));
      }
    );
  }

  sendIngredients() : void {
    //   this.recipe.ingredients.forEach(ingredient => {
    //       this.shoppingListService.addIngredient(ingredient);
    //   });
    this.shoppingListService.addIngredients(this.recipe.ingredients);
  }

  onEdit(): void {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDelete(): void {
    this.recipeService.deleteRecipe(this.recipe.id);
    this.router.navigate(['/recipes']);

  }
}
