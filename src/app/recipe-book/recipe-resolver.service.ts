import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { DataStorageService } from '../shared/data-storage.service';
import { Recipe } from './recipe.model';
import { RecipeService } from './recipe.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeResolverService implements Resolve<Recipe[]>{

  constructor(private dataStorage: DataStorageService, private recipeService: RecipeService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Recipe[] | Observable<Recipe[]> | Promise<Recipe[]> {
    if (this.recipeService.getRecipes().length > 0) return this.recipeService.getRecipes();
    let returnValue: Recipe[];
     this.dataStorage.fetchRecipes().subscribe({
       next: (recipes: Recipe[]) => returnValue = recipes,
     });
     return returnValue;
  }
}
