import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css'],
  animations: [
    trigger('hover', [
      state('normal', style({
        transform: 'translateX(0)',
      })),
      state('hovered', style({
        transform: 'translateX(30px)',
      })),
      transition('normal <=> hovered', animate(300)),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }),
        animate(300)
      ]),
      transition('* => void', [
        animate(300, style({
          opacity: 0,
          transform: 'translateX(100px)'
        }))
      ])
    ])
  ]
})
export class RecipeComponent {
   @Input() recipe: Recipe;
  constructor(private recipeService: RecipeService) { }
  public readonly states = ['normal', 'hovered'];
  private _state = 0;

  public get state() {
    return this._state;
  }
  onClick() : void {
      this.recipeService.recipeSelected.next(this.recipe);
  }

  changeState(): void {
    this._state++;
  }



}
