import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from '../recipe.model'
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css'],
})

export class RecipeListComponent implements OnInit, OnDestroy {
    recipes: Recipe[];
    recipesChangedSub: Subscription;

    hoverStates: string[];

  constructor(public recipeService: RecipeService) {}
  ngOnDestroy(): void {
    this.recipesChangedSub.unsubscribe();
  }

  ngOnInit(): void {
    this.recipes = this.recipeService.getRecipes();
    this.recipesChangedSub = this.recipeService.recipesChanged.subscribe((recipes) => this.recipes = recipes);
    this.hoverStates = [];
    this.recipes.forEach(element => this.hoverStates.push('normal'));
  }

  onHover(index: number): void {
    this.hoverStates[index] = 'hovered';
  }

  onLeave(index: number): void {
    this.hoverStates[index] = 'normal';
  }
}
