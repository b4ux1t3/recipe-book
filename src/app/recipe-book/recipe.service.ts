import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Recipe } from './recipe.model';

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();
    nextId = 1;
    private recipes: Recipe[];
    recipeSelected = new Subject<Recipe>();
  constructor() {
    this.recipes = [];
  }
  getCurrentId(): number {
    return this.nextId++;
  }
  onRecipesChanged(): void {
    this.recipesChanged.next(this.recipes.slice());
  }
  getRecipes() : Recipe[] {
      return this.recipes.slice();
  }

  getRecipe(id: number): Recipe {
    return this.recipes.filter(recipe => recipe.id === id)[0];
  }

  addRecipe(recipe: Recipe): Recipe {
    this.recipes.push(recipe);
    this.onRecipesChanged()
    return recipe;
  }

  updateRecipe(id: number, newRecipe: Recipe): Recipe {
    this.recipes[id - 1] = newRecipe;
    this.onRecipesChanged()
    return newRecipe;
  }

  deleteRecipe(id: number): Recipe {
    const deletedRecipe = this.recipes.splice(id - 1, 1)[0];
    this.onRecipesChanged();
    for (let i = 0; i < this.recipes.length; i++){
      this.recipes[i].id = i + 1;
    }
    this.nextId--;
    return deletedRecipe;
  }

  replaceAllRecipes(recipes: Recipe[]): void {
    this.recipes = recipes;
    this.onRecipesChanged();
    this.nextId = recipes.length + 1;
  }
}
