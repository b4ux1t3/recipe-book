import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataStorageService } from '../shared/data-storage.service';
import { Recipe } from './recipe.model';
import { RecipeService } from './recipe.service';

@Component({
  selector: 'app-recipe-book',
  templateUrl: './recipe-book.component.html',
  styleUrls: ['./recipe-book.component.css'],
  providers: []
})
export class RecipeBookComponent implements OnInit, OnDestroy {
  recipeSelected: Recipe = null;
  private recipeSelectedSub: Subscription;

  constructor(private recipeService: RecipeService, private dataStorage: DataStorageService){}
  ngOnInit(): void {
      this.recipeSelectedSub = this.recipeService.recipeSelected.subscribe(
          (recipe: Recipe) => {
              this.recipeSelected = recipe;
          }
      );

      this.dataStorage.fetchRecipes().subscribe();
  }

  ngOnDestroy(): void {
    this.recipeSelectedSub.unsubscribe();
  }
  updateCurrentRecipe(recipe: Recipe)
  {
      this.recipeSelected = recipe;
  }

}
