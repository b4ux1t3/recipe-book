import { HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { exhaustMap, take } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { User } from './user.model';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  private userSubscription: Subscription;
  private authorizationValue: string;

  constructor(private auth: AuthService) {
    this.userSubscription = this.auth.userSubject.subscribe((user: User) => {
      this.authorizationValue = user.token;
    });
  }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.toString().endsWith('users.json')) return next.handle(req);

    return this.auth.userSubject.pipe(
      take(1),
      exhaustMap(user => {
        if (!user) return next.handle(req);

        const moddedReq = req.clone({
          params: new HttpParams().set('auth', user.token)
        });
        return next.handle(moddedReq);
      })
    );
    // Someone wanna explain to me why this didn't work?
    // if (req.url.toString().endsWith('users.json')) return next.handle(req);
    // let moddedReq = this.auth.isAuthenticated ? req.clone({
    //   params: req.params.set('auth', this.authorizationValue)
    // }) : req.clone();
    // console.log(`Detected request:`);
    // console.log(moddedReq);

    // return next.handle(moddedReq);
  }
}
