import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { DataStorageService } from '../shared/data-storage.service';
import { FirebaseRecord } from '../shared/firebase-record.model';
import { FirebaseCredentials } from './firebase-credentials.model';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly signupUrl = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp';
  private readonly signinUrl = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword';
  private readonly apiKey    = 'AIzaSyDq0uPBunGGjNk31pr7tVeEjMf2G1lQBa0'; // YES, THIS IS VERY, VERY BAD. I know.
  private tokenTimer: ReturnType<typeof setInterval>;
  loggedInSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  userSubject: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  authMessageSubject: BehaviorSubject<authMessage> = new BehaviorSubject<authMessage>(null);

  private authCredentials: FirebaseCredentials;
  isAuthenticated: boolean = false;

  constructor(private http: HttpClient, private dataStorage: DataStorageService, private router: Router) {
  }
  loginUser(email: string, password: string): Observable<FirebaseCredentials> {
    return this.http.post<FirebaseCredentials>(
      this.signinUrl,
      {
        email: email,
        password: password,
        returnSecureToken: true,
      },
      {
        params: new HttpParams().append('key', this.apiKey),
      }
    ).pipe(
      catchError(this.handleError.bind(this)),
      map((response: FirebaseCredentials) => {
        this.loggedInSubject.next(true); // It is important that we call this here, because
                                         // otherwise the guard will think we're not signed in!
        this.dataStorage.getUserDisplayName(response.localId).subscribe({
          next: (displayName) => {
            const expiration =  new Date(new Date().getTime() + +response.expiresIn * 1000);
            this.updateUser(response.email, response.localId, displayName, response.idToken, expiration)
          },
          error: () => {
            const expiration =  new Date(new Date().getTime() + +response.expiresIn * 1000);
            this.updateUser(response.email, response.localId, 'Unknown', response.idToken, expiration)
          }
        });
        return response;
      }));
  }
  registerNewUser(email: string, password: string, displayName: string): Observable<FirebaseCredentials> {
    return this.http.post<FirebaseCredentials>(
      this.signupUrl,
      {
        email: email,
        password: password,
        returnSecureToken: true,
      },
      {
        params: new HttpParams().append('key', this.apiKey),
      }
    ).pipe(catchError(this.handleError.bind(this)), map((response) =>{
      const expiration =  new Date(new Date().getTime() + +response.expiresIn * 1000);
      this.updateUser(response.email, response.localId, 'Unknown', response.idToken, expiration);
      return response;
    }), tap((response: FirebaseCredentials) => this.dataStorage.saveUser(response, displayName)));
  };

  autoLogin(): void {
    const userData: {email: string, id: string, name: string, _token: string, _tokenExpiration: string} = JSON.parse(localStorage.getItem('userData'));
    if (!userData) return;

    const loadedUser = new User(userData.email, userData.id, userData._token, new Date(userData._tokenExpiration), userData.name);

    const expirationDuration: number = new Date(userData._tokenExpiration).getTime() - new Date().getTime();
    this.autoLogout(expirationDuration);
    if (loadedUser.token) {
      this.updateUser(null, null, null, null, null, loadedUser);
    }
  }

  autoLogout(expirationDuration: number): void {
    this.tokenTimer = setInterval(() => this.logout('You have been logged out due to inactivity.'), expirationDuration);
  }

  logout(message: string = 'You have logged out successfully.'): void {
    if (this.tokenTimer) {
      clearInterval(this.tokenTimer);
    }
    this.userSubject.next(null);
    this.loggedInSubject.next(false);
    this.authMessageSubject.next({message: message, code: 0});
    this.isAuthenticated = false;
    localStorage.removeItem('userData');

    this.router.navigate(['/auth']);
  }

  private handleError(errorResponse: HttpErrorResponse): Observable<never> {
    let errorMessage = 'An unknown error occurred.';
      if (!errorResponse.error || !errorResponse.error.error) {
        console.log(errorResponse);
        return throwError(errorMessage);
      }
      const error = errorResponse.error.error.message.split(' : ');
      console.log(error);
      switch (error[0]) {
        case 'EMAIL_NOT_FOUND':
        case 'INVALID_PASSWORD':
          errorMessage = 'No match could be found for that email/password combination';
          break;
        case 'USER_DISABLED':
          errorMessage = 'This user has been disabled.';
          break;
        case 'EMAIL_EXISTS':
          errorMessage = 'A user with that email already exists. Try to sign in.';
          break;
        case 'OPERATION_NOT_ALLOWED':
          errorMessage = 'This application does not allow password sign-ins.';
          break;
        case 'TOO_MANY_ATTEMPTS_TRY_LATER':
          errorMessage = error[1] ? error[1] : 'Too many attempts. Try again later';
          break;
      }
      this.authMessageSubject.next({message: errorMessage, code: 1});
      return throwError(errorMessage);
  }

  private updateUser(email: string, localId: string, displayName: string, idToken: string, expirationDate: Date, user: User = null): void {
    const newUser = user ? user : new User(email, localId, idToken, expirationDate, displayName);
    this.isAuthenticated = true;
    this.loggedInSubject.next(true);
    this.userSubject.next(newUser);
    localStorage.setItem('userData', JSON.stringify(newUser));
    if (expirationDate !== null) this.autoLogout(expirationDate.valueOf() - Date.now());
  }
}

export interface authMessage {
  message: string;
  code: number;
}

