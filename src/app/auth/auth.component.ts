import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { authMessage, AuthService } from './auth.service';
import { FirebaseCredentials } from './firebase-credentials.model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginMode = true;
  authMessage: authMessage = null;
  waitingForResponse = false;
  error: boolean = false;

  private authObservable: Observable<FirebaseCredentials>;

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    console.log('Entered Auth');
    this.auth.authMessageSubject.subscribe({
      next: (authMessage) => {
        if (authMessage) {
          this.authMessage = authMessage;
          this.error = authMessage.code !== 0;
        }
      },
    })
  }

  switchMode(): void {
    this.loginMode = !this.loginMode;
  }

  onSubmit(form: NgForm): void {
    this.waitingForResponse = true;

    if (this.loginMode) {
      this.authObservable = this.auth.loginUser(form.value.email, form.value.password);
    }
    else {
      this.authObservable = this.auth.registerNewUser(form.value.email, form.value.password, form.value.displayName);
    }

    this.authObservable.subscribe({
      next: (response) => {
        form.reset();
        this.waitingForResponse = false;
        this.loginRedirect();
      },
      error: (errorMessage) => {
        this.waitingForResponse = false;
      },
    });
  }

  private loginRedirect(): void {
    console.log('navigating away');
    this.router.navigate(['/recipes']);
  }

  clearError(): void {
    this.authMessage = null;
  }
}
