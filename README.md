# Recipe Book
An interactive recipe book running on the Angular SPA platform.

Fully-interactive version [here](https://b4ux1t3.gitlab.io/recipe-book).

Favicon Provided by [Wikimedia and Sebastian Meier](https://commons.wikimedia.org/wiki/File:Double_Burger_-_The_Noun_Project.svg).
